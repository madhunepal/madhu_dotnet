﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstHw
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?");
            string name = Console.ReadLine();
            Console.WriteLine("Hi there " + name + "!");
            Console.WriteLine("Where are you from?");
            string address = Console.ReadLine();
            Console.WriteLine("Oh you are from " + address + ".! I am also from " + address);
            Console.WriteLine("Great!!");
            Console.ReadKey();
        }
    }
}
